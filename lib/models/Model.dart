import 'package:flutter/cupertino.dart';

class Model {
   String name;
   String premiered;
   String imageUrl;
   String linkUrl;

  Model({
    @required this.name,
    @required this.premiered,
    @required this.imageUrl,
    @required this.linkUrl,
  });
}

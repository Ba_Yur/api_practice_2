import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:flutter/material.dart';

import 'models/Model.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Model> listOfModels = [];
  Model model;

  String param = 'cat';

  void getItem(param) async {
    final data = await http.get('http://api.tvmaze.com/search/shows?q=$param');
    print(json.decode(data.body));
    List<dynamic> listData = json.decode(data.body);

    setState(() {
      listOfModels = [];
      for (int i = 0; i < listData.length; i++) {
        Map<String, dynamic> modelData = listData[i];
        listOfModels.add(Model(
          name: modelData['show']['name'],
          premiered: modelData['show']['premiered'],
          imageUrl: modelData['show']['image']['medium'],
          linkUrl: modelData['show']['url'],
        ));
      }
    });
  }

  final controller = TextEditingController();

  @override
  void initState() {
    controller.addListener(printText);
    getItem(param);

    super.initState();
  }

  void printText() {
    print(controller.text);
    getItem(controller.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.deepOrangeAccent,
      appBar: AppBar(
        title: Text('API practice 2'),
      ),
      body: Stack(
        children: [ ListView.builder(
            itemCount: listOfModels.length,
            itemExtent: 250,
            itemBuilder: (BuildContext context, index) {
              return SizedBox(
                // height: 300,
                width: MediaQuery.of(context).size.width,
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Image.network(listOfModels[index].imageUrl, ),
                      Center(
                        child: SizedBox(
                          width: 200,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              RichText(
                                text: TextSpan(
                                    style: TextStyle(color:Colors.deepPurpleAccent, fontSize: 25, fontWeight: FontWeight.w700),
                                    children: [
                                      TextSpan(text: listOfModels[index].name)
                                    ]
                                ),
                              ),
                              RichText(
                                text: TextSpan(
                                  style: TextStyle(color:Colors.deepPurpleAccent, fontSize: 18),
                                  children: [
                                    TextSpan(text: 'Premiered:',
                                    ),
                                    TextSpan(text: listOfModels[index].premiered)
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }),
          Positioned(
            bottom: 0.0,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              color: Colors.white,
              child: TextField(
                controller: controller,
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
